package com.ancun.tom.spring.webmvc.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class OnClassCondition implements Condition{

	public boolean matches(ConditionContext context,
			AnnotatedTypeMetadata metadata) {
		return false;
	}

}
