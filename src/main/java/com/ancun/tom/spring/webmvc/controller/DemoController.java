package com.ancun.tom.spring.webmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ancun.tom.spring.webmvc.service.AService;

@RestController
public class DemoController {
	@Autowired(required=false)
	AService aservice;
	
	@RequestMapping("/test")
	public String test(){
		return "tom" + aservice.test();
	}
}
