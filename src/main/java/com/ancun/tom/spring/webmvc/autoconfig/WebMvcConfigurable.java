package com.ancun.tom.spring.webmvc.autoconfig;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.ancun.tom.spring.webmvc.condition.ConditionalOnClass;
import com.ancun.tom.spring.webmvc.service.AService;
import com.ancun.tom.spring.webmvc.service.BService;

@Configurable
@ComponentScan("com.ancun.tom.spring.webmvc.controller")
public class WebMvcConfigurable {

	@Bean
	@ConditionalOnClass(value=BService.class)
	public AService getDemoService(){
		return new AService();
	}
	
}
