package com.ancun.tom.spring.webmvc.autoconfig;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AutoConfigDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[]{WebMvcConfigurable.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
